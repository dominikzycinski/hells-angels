var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");

let road = new Image();
road.src = "img/road5.png";

var road_width = 350;
var road_height = 1160;

var motorcycler_width = 40;
var motorcycler_height = 50;

var lives = 3;
var score = 1;
var amo = 10;
var position = 0;

var police_width = 40;
var police_height = 50;

var motorcycler_speed = 4;
var police_speed = 1.5;
var enemy_rocket_speed = 3;

var motorcycler = new Map();
motorcycler.set("X", (canvas.width / 2) -20);
motorcycler.set("Y", canvas.height - 70);
motorcycler.set("width", motorcycler_width);
motorcycler.set("height", motorcycler_height);


var shots = [];
var shot_speed = 2;

var police = [];
var roads = [];
var collision_bool = true;

var background = [];


var right_pressed = false;
var left_pressed = false;
var up_pressed = false;
var down_pressed = false;
var space_pressed = false;


let motorcyclerImage = new Image();
motorcyclerImage.src = "img/motorcycler3.gif";

let rocket = new Image();
rocket.src = "img/carpolice.png";

let bullet = new Image();
bullet.src = "img/bullet.png";


let explosionImage = new Image();
explosionImage.src = "img/explosion.png";

let explosionSound = document.createElement('audio');
explosionSound.src = 'audio/shellExplosionSound.mp3';

let delay = 0;
let NUMBER_OF_SPRITES = 74; // the number of gameObjects in the gameObject image
let NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE = 9; // the number of columns in the gameObject image
let NUMBER_OF_ROWS_IN_SPRITE_IMAGE = 9; // the number of rows in the gameObject image	
let START_ROW = 0;
let START_COLUMN = 0;



let SPRITE_WIDTH = 100;//(explosionImage.width / NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE); 
let SPRITE_HEIGHT = 100;//(explosionImage.height / NUMBER_OF_ROWS_IN_SPRITE_IMAGE);



let sprites =[];

function drawMotorcycler() {
	ctx.drawImage(motorcyclerImage, motorcycler.get("X"), motorcycler.get("Y"), motorcycler.get("width"), motorcycler.get("height"));
}

function drawNewShot(shot_X, shot_Y) {
	var shot = new Map();
	shot.set("X", shot_X);
	shot.set("Y", shot_Y);
	shot.set("width", 5);
	shot.set("height", 15);
	shots.push(shot);
}

function drawshots() {
	for (var i = 0; i < shots.length; i++) {
		ctx.drawImage(bullet, shots[i].get("X"), shots[i].get("Y"), shots[i].get("width"), shots[i].get("height"));
	}
}
function drawPolice() {
	
		var number = Math.floor(Math.random() * 150);
	
	if (number  == 21) {
		var rocket = new Map();
		rocket.set("X", 100);
		rocket.set("Y", -20);
		rocket.set("width", police_width);
		rocket.set("height", police_height);
		police.push(rocket);

	}
	if (number  == 43) {
		var rocket = new Map();
		rocket.set("X", 155);
		rocket.set("Y", -10);
		rocket.set("width", police_width);
		rocket.set("height", police_height);
		police.push(rocket);
	}
	if (number  == 87) {
		var rocket = new Map();
		rocket.set("X", 210);
		rocket.set("Y",  -5);
		rocket.set("width", police_width);
		rocket.set("height", police_height);
		police.push(rocket);
	}

}

function drawrockets() {
	for (var i = 0; i < police.length; i++) {
		ctx.drawImage(rocket, police[i].get("X"), police[i].get("Y"), police[i].get("width"), police[i].get("height"));
	}
}

function moverPolice() {
	for (var i = 0; i < police.length; i++) {
		police[i].set("Y", police[i].get("Y") + enemy_rocket_speed);

		if (police[i].get("Y") > canvas.height) {
			score += 1;
			police.splice(i, 1);
		}
	}
}

function moverBackground() {
	for (var i = 0; i < roads.length; i++) {
		roads[i].set("Y", roads[i].get("Y") + enemy_rocket_speed);

		if (roads[i].get("Y") > canvas.height) {
			roads.splice(i, 1);
		}
	}
}

function move() {
	for (var i = 0; i < shots.length; i++) {
		shots[i].set("Y", shots[i].get("Y") - shot_speed);
		//Drops the rocket from the shots array when they are out of view
		if (shots[i].get("Y") < 0) {
			shots.splice(i, 1);
		}
	}

	 for (var j = 0; j < background.length; j++) {
		 
		background[j].set("Y", background[j].get("Y") + police_speed);
		console.log(background[j].get("Y"));
		if(background[background.length -1].get("Y") == 1.5){
			console.log("chuj");
			create_continuation_background();
		}
 		console.log(background[background.length -1].get("Y"));
	 	if (background[j].get("Y") > canvas.height) {
	 		background.splice(j, 1);
	 	}
	 }
}

function create_background() {
	var imageBackground = new Map();
		position = position +1;
		imageBackground.set("X", 0); 
		imageBackground.set("Y", 0);
		imageBackground.set("width", road_width);
		imageBackground.set("height", road_height);
		background.push(imageBackground);
}

function  create_continuation_background(){
	var imageBackground2 = new Map();

	    position = position +1;
		imageBackground2.set("X", 0); 
		imageBackground2.set("Y", -1160);
		imageBackground2.set("width", road_width);
		imageBackground2.set("height", road_height);
		background.push(imageBackground2);
}

function draw_background() {
	for (i = 0; i < background.length; i++) {
		var X = background[i].get("X");
		var Y = background[i].get("Y");
		ctx.drawImage(road, X, Y, road_width, road_height);
	}
}

function shoot_police_collision() {
	for (i = 0; i < police.length; i++) {
		var conflict_X = false;
		var conflict_Y = false;

		if (motorcycler.get("X") + motorcycler_width > police[i].get("X") && motorcycler.get("X") < police[i].get("X") + police_width) {
			conflict_X = true;
		}
		if (motorcycler.get("Y") < police[i].get("Y") + police_height && motorcycler.get("Y") > police[i].get("Y")) {
			conflict_Y = true;
		}
		if (conflict_X && conflict_Y) {
			collision_bool = false;
			lives -= 1;

			var sprite = new Map();
			sprite.set("row",START_ROW);
			sprite.set("column",START_COLUMN);
			sprite.set("X",motorcycler.get("X"));
			sprite.set("Y",motorcycler.get("Y"));
			sprite.set("isFirstCallOfUpdateState",true);
			sprite.set("currentgameObject",0);
			sprites.push(sprite);

			police = [];
			background = [];
			shots = [];
			return;
		}
	}
	collision_bool = true;
}

function collision_detector(first, second) {
	var x1 = first.get("X");
	var y1 = first.get("Y");
	var width1 = first.get("width");
	var height1 = first.get("height");
	var x2 = second.get("X");
	var y2 = second.get("Y");
	var width2 = second.get("width");
	var height2 = second.get("height");

	if (x2 > x1 && x2 < x1 + width1 || x1 > x2 && x1 < x2 + width2) {
		if (y2 > y1 && y2 < y1 + height1 || y1 > y2 && y1 < y2 + height2) {
			return true;
		}
	} else {
		return false;
	}
}

function rocket_police_collision() {
	for (var i = 0; i < police.length; i++) {
		var policeCar = police[i];
		for (var j = 0; j < shots.length; j++) {
			var rocket = shots[j];
			if (collision_detector(policeCar, rocket)) {
			
				var sprite = new Map();
			sprite.set("row",START_ROW);
			sprite.set("column",START_COLUMN);
			sprite.set("X",policeCar.get("X"));
			sprite.set("Y",policeCar.get("Y"));
			sprite.set("isFirstCallOfUpdateState",true);
			sprite.set("currentgameObject",0);
			sprites.push(sprite);

				shots.splice(j, 1);
				police.splice(i, 1);
				score += 2;
			}
		}
	}
}

function drawInfo() {
	ctx.font = "bold 15px Gill Sans MT";
	ctx.fillStyle = "red";
	ctx.fillText("LIVES: " + lives, 10, 22);
	ctx.fillText("SCORE: " + score, 265, 22)
	ctx.fillText("AMO: " + amo, 265, 42)
}

document.addEventListener('touchstart', handleTouchStart, false);
document.addEventListener('touchmove', handleTouchMove, false);

var xDown = null;
var yDown = null;

function getTouches(evt) {
	return evt.touches || 
		evt.originalEvent.touches; 
}

function handleTouchStart(evt) {
	const firstTouch = getTouches(evt)[0];
	xDown = firstTouch.clientX;
	yDown = firstTouch.clientY;

	canvas.onclick = function () {
		if(amo>0){
		amo = amo -1;
		drawNewShot(motorcycler.get("X") + 15, motorcycler.get("Y") - 30);
		}
	};
};

function handleTouchMove(evt) {
	if (!xDown || !yDown) {
		return;
	}
	canvas.onclick = function () {
		right_pressed = false;
		left_pressed = false;
		up_pressed = false;
		down_pressed = false;
	};

	var xUp = evt.touches[0].clientX;
	var yUp = evt.touches[0].clientY;

	var xDiff = xDown - xUp;
	var yDiff = yDown - yUp;

	if (Math.abs(xDiff) > Math.abs(yDiff)) {
		if (xDiff > 0) {
			right_pressed = false;
			left_pressed = true;
			
		} else {
			left_pressed = false
			right_pressed = true;
		}
	} else {
		if (yDiff > 0) {
			motorcycler_speed = motorcycler_speed + 4 ;
			police_speed = police_speed + 4;
			enemy_rocket_speed = enemy_rocket_speed + 4;

		} else {
			motorcycler_speed = 4;
			police_speed = 1.5;
			enemy_rocket_speed = 3;
		}
	}
	xDown = null;
	yDown = null;
}

function draw_sprite()
{
	for (var i = 0; i < sprites.length; i++)
	{
		if (sprites[i].get("isFirstCallOfUpdateState"))
        {
            explosionSound.currentTime = 0;
            explosionSound.play();
            sprites[i].set("isFirstCallOfUpdateState",false);
        }
		sprites[i].set("currentgameObject",sprites[i].get("currentgameObject")+1);
        if (sprites[i].get("currentgameObject") == 73)
        {
            sprites.splice(i, 1);
			continue;
        }else
		{
			ctx.drawImage(explosionImage, sprites[i].get("column") * SPRITE_WIDTH, sprites[i].get("row") * SPRITE_WIDTH, SPRITE_WIDTH, SPRITE_HEIGHT, sprites[i].get("X")-20, sprites[i].get("Y"), 100, 100);
			
		}
        
        sprites[i].set("column",sprites[i].get("column")+1);
        if (sprites[i].get("column") >= NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE)
        {
			sprites[i].set("column",0);
			sprites[i].set("row",sprites[i].get("row")+1);
        }
		
	}
}  

function draw() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	 draw_background();
	drawInfo();
	drawMotorcycler();
	drawrockets();
	drawshots();
	move();
	// shoot_police_collision();
	// rocket_police_collision();
	moverPolice();
	draw_sprite();

	difficult =14;
	if (right_pressed && motorcycler.get("X") + motorcycler_width < canvas.width - 90) {
		motorcycler.set("X", motorcycler.get("X") + 60);
		right_pressed = false;
	}
	if (left_pressed && motorcycler.get("X") > 145) {
		motorcycler.set("X", motorcycler.get("X") - 60); 
		left_pressed = false;
	}
	if (police.length < difficult) {
		drawPolice();
	}
	if(score % 50 ==0){
		amo = amo +10;
		score = score +1;
		difficult = difficult +3;
	}
	 if (background.length < 1) {
		 create_background();
		//  create_continuation_background();
	
	 }

	// if(background[background.length -1].get("Y")==1.5)
	// {
	//  	 create_continuation_background();
	// }
	if (!collision_bool && lives < 1) {
		alert("THE END, YOUR SCORE: " + score);
		document.location.reload()
	}
	
	requestAnimationFrame(draw);
}

draw();
